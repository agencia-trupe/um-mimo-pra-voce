<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicial_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'banners';

		$this->dados = array();
		$this->dados_tratados = array();
	}

	function pegarTexto($id = false){
		if($id){
			$qry = $this->db->get_where('home', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else
			return $this->db->get('home')->result();
	}

	function pegarBanners($id = false){
		if($id){
			$qry = $this->db->get_where('banners', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else
			return $this->db->order_by('ordem', 'asc')->get('banners')->result();
	}

	function atualizaTexto($id){
		return $this->db->set('texto', $this->input->post('texto'))
						->where('id', $id)
						->update('home');
	}

	function inserirBanner(){
		return $this->db->set('texto', $this->input->post('texto'))
						->set('imagem', $this->sobeImagem())
						->set('destino', $this->input->post('destino'))
						->insert('banners');
	}

	function atualizaBanner($id){

		$imagem = $this->sobeImagem();

		if($imagem)
			$this->db->set('imagem', $imagem);
				
		return $this->db->set('texto', $this->input->post('texto'))
						->set('destino', $this->input->post('destino'))
						->where('id', $id)
						->update('banners');
	}

	function excluirBanner($id){
		return $this->db->where('id', $id)->delete('banners');
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/banners/',
			'x' => '450',
			'y' => '340',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '',
		  'max_width' => '2000',
		  'max_height' => '1000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE)
		                ->resize_crop(140, 140)
		                ->save($original['dir'].'thumbs/'.$filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}