<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parceiros_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'parceiros';
		
		$this->dados = array(
			'titulo',
			'imagem',
			'destino'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}	

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/parceiros/',
			'campo' => 'userfile'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
	        	$this->image_moo
	        		 ->set_background_colour('#FFFFFF')
	                 ->load($original['dir'].$filename)
	                 ->resize(240, 180, TRUE)
	                 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}