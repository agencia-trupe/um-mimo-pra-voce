<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'servicos';

		$this->dados = array(
			'titulo',
			'slug',
			'texto'
		);
		$this->dados_tratados = array(
			'slug' => url_title($this->input->post('titulo'))
		);
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}

}