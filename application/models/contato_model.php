<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';

		$this->dados = array('telefone', 'endereco', 'email', 'maps', 'twitter', 'facebook');
		$this->dados_tratados = array(
			'maps' => limpaGMaps($this->input->post('maps'))
		);
		
	}

}