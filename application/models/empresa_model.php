<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'empresa';

		$this->imagemOriginal = array(
			'dir' => '_imgs/empresa/',
			'x' => '370',
			'y' => '550',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);
		
		$this->dados = array(
			'texto1',
			'texto2',
			'texto_destaque',
			'imagem'
		);
		$this->dados_tratados = array( 'imagem' => $this->sobeImagem() );
	}

}