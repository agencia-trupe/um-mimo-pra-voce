<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'produtos';

		$this->dados = array('titulo', 'imagem', 'slug');
		$this->dados_tratados = array(
			'titulo' => $this->input->post('titulo'),
			'slug' => url_title($this->input->post('titulo'), '_', TRUE),
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarPorCategoria($campo, $identificador){
$query = <<<STR
SELECT produtos.* FROM produtos
LEFT JOIN rel_produtos_categorias ON produtos.id = rel_produtos_categorias.id_produtos
LEFT JOIN produtos_categorias ON rel_produtos_categorias.id_produtos_categorias = produtos_categorias.id
WHERE produtos_categorias.$campo = '$identificador'
STR;
		return $this->db->query($query)->result();
	}

	function categoriasDoProduto($id, $retornar_array = TRUE){
		$query = $this->db->get_where('rel_produtos_categorias', array('id_produtos' => $id))->result();

		foreach ($query as $key => $value) {
			$array[] = $value->id_produtos_categorias;
		}

		if($retornar_array)
			return $array;
		else
			return $query;
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}

		$insert = $this->db->insert($this->tabela);

		$id = $this->db->insert_id();

		$categorias = $this->input->post('categorias');
		if($categorias){
			foreach ($categorias as $key => $value) {
				$this->db->set('id_produtos', $id)->set('id_produtos_categorias', $value)->insert('rel_produtos_categorias');
			}
		}

		return $insert;
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			$update = $this->db->where('id', $id)->update($this->tabela);

			$this->db->where('id_produtos', $id)->delete('rel_produtos_categorias');

			$categorias = $this->input->post('categorias');
			if($categorias){
				foreach ($categorias as $key => $value) {
					$this->db->set('id_produtos', $id)->set('id_produtos_categorias', $value)->insert('rel_produtos_categorias');
				}
			}

			return $update;
		}
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			$this->db->where('id_produtos', $id)->delete('rel_produtos_categorias');
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

	function pegarCategorias($id = false){
		if($id){
			$qry = $this->db->get_where('produtos_categorias', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else
			return $this->db->order_by('ordem', 'asc')->get('produtos_categorias')->result();
	}

	function inserir_categoria(){
		return $this->db->set('titulo', $this->input->post('titulo'))
						->set('slug', url_title($this->input->post('titulo'), '_', TRUE))
						->insert("produtos_categorias");
	}

	function alterar_categoria($id){
		return $this->db->set('titulo', $this->input->post('titulo'))
						->set('slug', url_title($this->input->post('titulo'), '_', TRUE))
						->where('id', $id)
						->update("produtos_categorias");
	}

	function excluir_categoria($id){
		if($this->pegarCategorias($id) !== FALSE){
			return $this->db->where('id', $id)->delete("produtos_categorias");
		}
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/produtos/',
			'campo' => 'userfile'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        $this->image_moo
		             ->load($original['dir'].$filename)
		             ->resize_crop(140, 140)
		             ->save($original['dir'].'thumbs/'.$filename, TRUE);	
		        
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}