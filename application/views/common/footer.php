  </div> <!-- fim da div conteudo -->

  <footer>

    <div class="telefone">
      Nosso telefone:<br>
      <span class="num"><?=$contato[0]->telefone?></span>
    </div>

    <div class="assinatura">
      &copy; <?=Date('Y')?> "Um mimo pra você" - Todos os direitos reservados<br>
      <a href="http://www.trupe.net" target="_blank" title="Criação de Site : Trupe Agência Criativa">
        <span>Criação de Site : Trupe Agência Criativa</span>
        <img src="_imgs/layout/trupe.png">
      </a>
    </div>
  
  </footer>

</div> <!-- fim da div main -->
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('cycle','fancybox','front'))?>
  
</body>
</html>
