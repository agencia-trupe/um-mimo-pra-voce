<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<div id="menu-lateral">

		<ul>
			<?php if ($categorias): ?>
				<?php foreach ($categorias as $key => $value): ?>
					<li>&raquo; <a href="produtos/categorias/<?=$value->slug?>" <?if(isset($categoria_ativa_slug) && $categoria_ativa_slug == $value->slug)echo" class='ativo'"?> title="<?=$value->titulo?>"><?=$value->titulo?></a></li>		
				<?php endforeach ?>
			<?php endif ?>
		</ul>

	</div><div id="conteudo">

		<header>

			<img id="img-logo" src="_imgs/layout/logo.png" alt="Um mimo pra você!">

			<nav>
				<ul>
					<li id="mn-home"><a href="home" title="Página Inicial" <?if($this->router->class=='home')echo" class='ativo'"?>>home</a></li>
					<li id="mn-empresa"><a href="empresa" title="Empresa" <?if($this->router->class=='empresa')echo" class='ativo'"?>>empresa</a></li>
					<li id="mn-produtos"><a href="produtos" title="Produtos" <?if($this->router->class=='produtos')echo" class='ativo'"?>>produtos</a></li>
					<li id="mn-servicos"><a href="servicos" title="Servicos" <?if($this->router->class=='servicos')echo" class='ativo'"?>>serviços</a></li>
					<li id="mn-parceiros"><a href="parceiros" title="Parceiros" <?if($this->router->class=='parceiros')echo" class='ativo'"?>>parceiros</a></li>
					<li id="mn-contato"><a href="contato" title="Contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
				</ul>

				<?php if ($contato[0]->facebook): ?>
					<a href="<?=$contato[0]->facebook?>" target="_blank" id="link-facebook"><img src="_imgs/layout/facebook.png"></a>	
				<?php endif ?>
				<?php if ($contato[0]->twitter): ?>
					<a href="<?=$contato[0]->twitter?>" target="_blank" id="link-twitter"><img src="_imgs/layout/twitter.png"></a>	
				<?php endif ?>

				<div id="frase-topo">
					Lembranças personalizadas para todas as comemorações!
				</div>
			</nav>
			
		</header>



