<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/update_banner/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<textarea name="texto" style="width:300px; height:90px; resize:none;"><?=$registro->texto?></textarea></label>

		<label>Imagem<br>
		<a href="_imgs/banners/<?=$registro->imagem?>" target="_blank">
			<img src='_imgs/banners/thumbs/<?=$registro->imagem?>'>
		</a>
		<br>
		<input type="file" name="userfile"></label>

		<label>Destino do Link<br>
		<input type="text" name="destino" value="<?=$registro->destino?>"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/insert_banner/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<textarea name="texto" style="width:300px; height:90px; resize:none;"></textarea></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<label>Destino do Link<br>
		<input type="text" name="destino"></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>