<h1><?=$titulo?></h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Endereço<br>
		<textarea name="endereco" style="width:300px; height:90px; resize:none;"><?=$registro->endereco?></textarea></label>

		<label>Telefone<br>
		<input type="text" name="telefone" value="<?=$registro->telefone?>"></label>

		<label>E-mail<br>
		<input type="email" name="email" value="<?=$registro->email?>"></label>

		<label>Twitter<br>
		<input type="text" name="twitter" value="<?=$registro->twitter?>"></label>

		<label>Facebook<br>
		<input type="text" name="facebook" value="<?=$registro->facebook?>"></label>

		<label>Link do Google Maps<br>
		<input type="text" name="maps" value="<?=htmlentities($registro->maps)?>"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>