<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($this->router->method=='index')echo' active'?>">Listar Produtos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add <?if($this->router->method=='form')echo' active'?>">Inserir Produto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista <?if($this->router->method=='categorias')echo' active'?>">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form_categorias')?>" class="add <?if($this->router->method=='form_categorias')echo' active'?>">Inserir Categoria</a>
</div>

<div class="submenu">
	<h3>Filtrar por Categoria:</h3>
	<?php foreach ($categorias as $key => $value): ?>
		<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$value->slug)?>" class="lista <?if($filtro==$value->slug)echo' active'?>"><?=$value->titulo?></a>	
	<?php endforeach ?>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=$value->titulo?></td>
					<td><img src="_imgs/produtos/thumbs/<?=$value->imagem?>"></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?else:?>

	<h2>Nenhum Produto Cadastrado</h2>

<?endif;?>
