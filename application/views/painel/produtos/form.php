<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($this->router->method=='index')echo' active'?>">Listar Produtos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add <?if($this->router->method=='form')echo' active'?>">Inserir Produto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista <?if($this->router->method=='categorias')echo' active'?>">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form_categorias')?>" class="add <?if($this->router->method=='form_categorias')echo' active'?>">Inserir Categoria</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>"></label>

		<label>Imagem<br>
		<a href="_imgs/produtos/<?=$registro->imagem?>" target="_blank">
			<img src="_imgs/produtos/thumbs/<?=$registro->imagem?>">
		</a>
		<br>
		<input type="file" name="userfile"></label>

		<label style="margin-bottom:0;">Categorias</label>
		<?php foreach ($categorias as $key => $value): ?>
			<label style="margin-bottom:0;"><input type="checkbox" name="categorias[]" value="<?=$value->id?>" <?if(in_array($value->id, $categorias_do_produto))echo" checked"?>><?=$value->titulo?></label>
		<?php endforeach ?>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo"></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<label style="margin-bottom:0;">Categorias</label>
		<?php foreach ($categorias as $key => $value): ?>
			<label style="margin-bottom:0;"><input type="checkbox" name="categorias[]" value="<?=$value->id?>"><?=$value->titulo?></label>
		<?php endforeach ?>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>