<h1><?=$titulo?></h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto 1<br>
		<textarea name="texto1" class="pequeno basico"><?=$registro->texto1?></textarea></label>

		<label>Texto 2<br>
		<textarea name="texto2" class="pequeno basico"><?=$registro->texto2?></textarea></label>

		<label>Imagem<br>
		<img src="_imgs/empresa/<?=$registro->imagem?>" style="width:150px;">
		<br>
		<input type="file" name="userfile"></label>

		<label>Texto de Destaque<br>
		<textarea name="texto_destaque" class="pequeno basico"><?=$registro->texto_destaque?></textarea></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto 1<br>
		<textarea name="texto1" class="pequeno basico"></textarea></label>

		<label>Texto 2<br>
		<textarea name="texto2" class="pequeno basico"></textarea></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<label>Texto de Destaque<br>
		<textarea name="texto_destaque" class="pequeno basico"></textarea></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>