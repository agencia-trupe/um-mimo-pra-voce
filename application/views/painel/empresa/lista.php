<h1><?=$titulo?></h1>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Texto</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=word_limiter($value->texto1, 10)?></td>
					<td><img style="width:150px;" src="_imgs/empresa/<?=$value->imagem?>"></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>