
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - <?=CLIENTE?></a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><a href="<?=base_url('painel/usuarios')?>" <?if($this->router->class=='usuarios')echo " class='active'"?>>Usuários</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<ul>
		    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
		    <li><a href="<?=base_url('painel/inicial')?>" <?if($this->router->class=='inicial')echo " class='active'"?>><span>Página Inicial</span></a></li>
		    <li><a href="<?=base_url('painel/empresa')?>" <?if($this->router->class=='empresa')echo " class='active'"?>><span>Empresa</span></a></li>
		    <li><a href="<?=base_url('painel/produtos')?>" <?if($this->router->class=='produtos')echo " class='active'"?>><span>Produtos</span></a></li>
		    <li><a href="<?=base_url('painel/servicos')?>" <?if($this->router->class=='servicos')echo " class='active'"?>><span>Serviços</span></a></li>
		    <li><a href="<?=base_url('painel/parceiros')?>" <?if($this->router->class=='parceiros')echo " class='active'"?>><span>Parceiros</span></a></li>
		    <li><a href="<?=base_url('painel/contato')?>" <?if($this->router->class=='contato')echo " class='active'"?>><span>Contato</span></a></li>
		</ul>
	</div>
	<!-- End Navigation -->
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>