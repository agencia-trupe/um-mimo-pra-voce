<section>

	<h1>contato</h1>

	<div class="container">

		<div class="coluna-1">
			<div class="endereco">
				<?=nl2br($contato[0]->endereco)?>
			</div>
			<?php if ($contato[0]->maps): ?>
				<?=viewGMaps($contato[0]->maps, 400, 303)?>	
			<?php endif ?>
		
		</div><div class="coluna-2">
		
			<div class="telefone">
				<?=$contato[0]->telefone?>
			</div>
			<a class="mail-link" href="mailto:<?=$contato[0]->email?>"><?=$contato[0]->email?></a>

			<?php if (!$this->session->flashdata('envio_status')): ?>
				
				<form method="post" action="contato/enviar">
					<input type="text" name="nome" placeholder="Nome" required>
					<input type="email" name="email" placeholder="E-mail" required>
					<input type="text" name="telefone" placeholder="Telefone">
					<textarea name="mensagem" placeholder="Mensagem"></textarea>
					<div class="submit-placeholder">
						<input type="submit" value="enviar">
					</div>
				</form>

			<?php else:?>

				<h2 style="margin-top:80px;">Mensagem enviada com sucesso!<br>Entraremos em contato!</h2>

			<?php endif; ?>

		</div>

	</div>

</section>