<section>

	<h1>serviços</h1>

	<?php if ($servicos): ?>

		<?php foreach ($servicos as $key => $value): ?>
			
			<a href="#" title="<?=$value->titulo?>" class="abre-texto" data-id="texto-<?=$key?>"><?=$value->titulo?></a>
			<div class="texto fechado" id="texto-<?=$key?>">
				<?=$value->texto?>
			</div>

		<?php endforeach ?>
		
	<?php else: ?>

		<h2>Nenhum serviço encontrado!</h2>
		
	<?php endif ?>
	

</section>