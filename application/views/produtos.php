<section>

	<h1>exibindo lembranças para: <?=$categoria_ativa_titulo?></h1>

	<?php if ($produtos): ?>

		<div class="container">
			<?php foreach ($produtos as $key => $value): ?><a href="_imgs/produtos/<?=$value->imagem?>" title="<?=$value->titulo?>" class="fancy<?if(($key+1)%5==0 && $key > 0)echo" ultimo"?>">
					<img src="_imgs/produtos/thumbs/<?=$value->imagem?>">
					<?=$value->titulo?>
				</a><?php endforeach ?>
		</div>

	<?php else: ?>

		<h2>Nenhum Produto encontrado!</h2>
		
	<?php endif ?>

</section>