<section>

	<h1>parceiros</h1>

	<?php if ($parceiros): ?>

		<div class="container">
			<?php foreach ($parceiros as $key => $value): ?><a href="<?=$value->destino?>" title="<?=$value->titulo?>" target="_blank" class="link-parceiros<?if(($key + 1)%3==0 && $key > 0)echo" ultimo"?>"><img src="_imgs/parceiros/<?=$value->imagem?>"></a><?php endforeach ?>
		</div>
		
	<?php else: ?>

		<h2>Nenhum parceiro encontrado!</h2>
		
	<?php endif ?>

</section>