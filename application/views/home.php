<section>

	<div id="slides">

		<?php if ($slides): ?>

			<?php foreach ($slides as $key => $value): ?>

				<div class="slide" <?if($key > 0)echo" style='display:none;'"?>>
					<a href="<?=$value->destino?>" title="<?=$value->texto?>">
						<img src="_imgs/banners/<?=$value->imagem?>">
						<div class="slide-texto">
							<?=nl2br($value->texto)?>
						</div>
					</a>
				</div>

			<?php endforeach ?>

		<?php endif ?>

	</div>

	<div id="texto-home">
		<?=$texto[0]->texto?>
	</div>

	<div id="navegacao"></div>

</section>