<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

    	$qry = $this->db->get('empresa')->result();
    	$data['empresa'] = $qry[0];

   		$this->load->view('empresa', $data);
    }

}