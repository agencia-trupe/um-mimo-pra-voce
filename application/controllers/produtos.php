<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('produtos_model', 'model');
    }

    function index(){
    	redirect('produtos/categorias');
    }

    function categorias($slug_categoria = false){

    	if(!$slug_categoria){
    		$qry = $this->db->order_by('ordem', 'asc')->get('produtos_categorias', 1, 0)->result();
    		$slug_categoria = $qry[0]->slug;
    		$titulo_categoria = $qry[0]->titulo;
    	}else{
    		$qry = $this->db->get_where('produtos_categorias', array('slug' => $slug_categoria))->result();
    		if(!$qry)
    			redirect('home');
    		$slug_categoria = $qry[0]->slug;
    		$titulo_categoria = $qry[0]->titulo;
    	}

    	$menu['categorias'] = $this->db->order_by('ordem', 'asc')->get('produtos_categorias')->result();
    	$menu['categoria_ativa_slug'] = $slug_categoria;
    	$data['categoria_ativa_titulo'] = $titulo_categoria;
    	$data['produtos'] = $this->model->pegarPorCategoria('slug', $slug_categoria);

    	$this->load->view('common/menu', $menu);
    	$this->load->view('produtos', $data);
    }

    function _output($output){
        echo $this->load->view('common/header', $this->headervar, TRUE).
             $output.
             $this->load->view('common/footer', $this->footervar, TRUE);
    }


}