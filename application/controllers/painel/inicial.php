<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicial extends MY_Admincontroller {

   	function __construct(){
   		parent::__construct();

   		$this->load->model('inicial_model', 'model');
   		$this->titulo = 'Página Inicial';
   	}

   	function texto(){
        $data['registros'] = $this->model->pegarTexto();

        $data['titulo'] = "Texto da Página Inicial";
        $data['unidade'] = "Texto";
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/'.$this->router->class.'/lista_texto', $data);
   	}

   	function form_texto($id = false){
        if($id){
            $data['registro'] = $this->model->pegarTexto($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class.'/texto');
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = 'Editar Texto da Página Inicial';
        $data['unidade'] = 'Texto';
        $this->load->view('painel/'.$this->router->class.'/form_texto', $data);   		
   	}

   	function update_texto($id){
        if($this->model->atualizaTexto($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Texto alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Texto');
        }

        redirect('painel/'.$this->router->class.'/texto', 'refresh');
   	}

   	function banners(){
        $data['registros'] = $this->model->pegarBanners();
        $data['num_banners'] = $this->db->get('banners')->num_rows();

        $data['titulo'] = "Banners da Página Inicial";
        $data['unidade'] = "Banner";
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/'.$this->router->class.'/lista_banners', $data);   		
   	}

    function form_banners($id = false){
        if($id){
            $data['registro'] = $this->model->pegarBanners($id);
            $data['titulo'] = 'Alterar Banner da Página Inicial';
            if(!$data['registro'])
                redirect('painel/'.$this->router->class.'/banners');
        }else{
            $data['registro'] = FALSE;
            $data['titulo'] = 'Inserir Banner na Página Inicial';
        }
        
        $data['unidade'] = 'Banner';
        $data['num_banners'] = $this->db->get('banners')->num_rows();
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
        $this->load->view('painel/'.$this->router->class.'/form_banners', $data);       
    }

    function insert_banner(){
        if($this->model->inserirBanner()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/banners', 'refresh');
    }

    function update_banner($id){
        if($this->model->atualizaBanner($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Texto alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Texto');
        }

        redirect('painel/'.$this->router->class.'/banners', 'refresh');
    }

    function delete_banner($id){
        if($this->model->excluirBanner($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/banners', 'refresh');
    }    

}