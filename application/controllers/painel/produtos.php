<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Admincontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('produtos_model', 'model');
    }


    function index($filtro = false){
        if($filtro){
            $data['titulo'] = 'Produtos por Categoria - '.$filtro;
            $data['registros'] = $this->model->pegarPorCategoria('slug', $filtro);
        }else{
            $data['registros'] = $this->model->pegarTodos();
            $data['titulo'] = 'Todos os Produtos';
        }

        $data['filtro'] = $filtro;

        $data['categorias'] = $this->model->pegarCategorias();
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            $data['titulo'] = 'Alterar Produto';
            $data['categorias_do_produto'] = $this->model->categoriasDoProduto($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
        	$data['titulo'] = 'Inserir Produto';
            $data['registro'] = FALSE;
        }

        $data['categorias'] = $this->model->pegarCategorias();
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function categorias(){
        $data['registros'] = $this->model->pegarCategorias();

        $data['titulo'] = 'Categorias';
        $data['unidade'] = 'Categoria';
        $this->load->view('painel/'.$this->router->class.'/lista_categorias', $data);
    }

    function form_categorias($id = false){
        if($id){
            $data['registro'] = $this->model->pegarCategorias($id);
            $data['titulo'] = 'Alterar Categorias';
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
        	$data['titulo'] = 'Inserir Categorias';
            $data['registro'] = FALSE;
        }

        $data['unidade'] = 'Categoria';
        $this->load->view('painel/'.$this->router->class.'/form_categorias', $data);    	
    }

    function adicionar_categorias(){
        if($this->model->inserir_categoria()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function alterar_categorias($id){
        if($this->model->alterar_categoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function excluir_categorias($id){
        if($this->model->excluir_categoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

}