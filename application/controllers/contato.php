<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$data['contato'] = $this->db->get('contato')->result();
   		$this->load->view('contato', $data);
    }

    function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        
        //$emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = '';
        $emailconf['smtp_user'] = '';
        $emailconf['smtp_pass'] = '';
        $emailconf['smtp_port'] = 587;
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        
        

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_fone = $this->input->post('telefone');
        $u_msg = $this->input->post('mensagem');

        $from = 'noreply@ummimopravoce.com.br';
        $fromname = 'Contato via Site';
        $to = 'contato@ummimopravoce.com.br';

        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Contato pelo site';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_fone</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        
        if($bc)
            $this->email->bc($bc);
        if($bcc)
            $this->email->bcc($bcc);
        
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->session->set_flashdata('envio_status', TRUE);
        
        $this->email->send();
        redirect('contato', 'refresh');    	
    }

}