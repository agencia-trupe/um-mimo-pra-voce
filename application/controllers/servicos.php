<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$data['servicos'] = $this->db->order_by('ordem')->get('servicos')->result();
   		$this->load->view('servicos', $data);
   }

}