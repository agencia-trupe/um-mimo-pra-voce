<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parceiros extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$data['parceiros'] = $this->db->order_by('ordem', 'asc')->get('parceiros')->result();
   		$this->load->view('parceiros', $data);
   }

}