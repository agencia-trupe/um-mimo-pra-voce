<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$data['slides'] = $this->db->order_by('ordem', 'asc')->get('banners')->result();
   		$data['texto'] = $this->db->get('home')->result();
   		$this->load->view('home', $data);
   }

}