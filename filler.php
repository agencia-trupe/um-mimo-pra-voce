<?php

/*
TO DO:
Trocar palavras aleatórias por banco de palavras (api?)
Adicionar outros campos do mysql
*/

class Filler{

	var $table, $fields, $CI, $query, $lorem, $affected_rows = 0;

	function __construct($table = false){
		//$this->CI =& get_instance();

		if(!$table){
			die('You must specify a table');
		}else{
			$this->table = $table;
		}

		$this->lorem = simplexml_load_file("http://www.lipsum.com/feed/xml?amount=1&what=paras&start=0")->lipsum;

		mysql_connect('localhost', 'root', 'senhatrupe');
		mysql_select_db('mimo');
	}

	function fill($num_rows = 10){
		$this->_getFields();
		for ($i=0; $i < $num_rows; $i++) { 
			$this->_prepareQuery();
			$this->_execute();
		}
		echo ($this->affected_rows != 1)? $this->affected_rows." rows inserted into table ".$this->table : $this->affected_rows." row inserted into table ".$this->table;
	}

	private function _getFields(){

		$query_fields = "SHOW COLUMNS FROM ".$this->table;
		$exec_fields = mysql_query($query_fields);

		while($array = mysql_fetch_assoc($exec_fields)){

			if($array['Extra'] != 'auto_increment'){

				preg_match("/\([0-9]*\)/i", $array['Type'], $size);
				$size[0] = trim($size[0], ')');
				$size[0] = trim($size[0], '(');

				$fields[] = array(
					'name' => $array['Field'],
					'type' => preg_replace("/\([0-9]*\)/i", '', $array['Type']),$array['Type'],
					'size' => $size[0]
				);

			}
		}
		$this->fields = $fields;
	}

	private function _prepareQuery(){
		$this->query = "INSERT INTO ".$this->table." (";
		foreach ($this->fields as $key => $value) {
			if($key == 0)
				$this->query .= $value['name'];
			else
				$this->query .= ', '.$value['name'];
		}
		$this->query .= ") VALUES(";
		foreach ($this->fields as $key => $value) {
			if($key == 0)
				$this->query .= "'".$this->_randomValue($value['type'], $value['size'])."'";
			else
				$this->query .= ", '".$this->_randomValue($value['type'], $value['size'])."'";
		}
		$this->query .= ")";
	}

	private function _randomValue($type, $size = 10){
		switch($type){
			case 'int':
				return rand(0, 1000);
				break;
			case 'date':
				return rand(1900, 2020).'-'.rand(1,12).'-'.rand(1, 28);
				break;
			case 'varchar':
				return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $size);
				break;
			case 'text':
				return $this->_lorem();
				break;
			default:
				die('Inconsistent type of field : '.$type);
		}
	}

	private function _lorem(){
		if($this->lorem)
			return $this->lorem;
		else{
			return simplexml_load_file("http://www.lipsum.com/feed/xml?amount=1&what=paras&start=0")->lipsum;
		}

	}

	private function _execute(){
		if(!mysql_query($this->query)){
			die('Erro ao inserir query');
		}else{
			$this->affected_rows++;
		}
	}
}

$filler = new Filler('contato');
$filler->fill(1);

?>