$('document').ready( function(){

	$('#slides').cycle({
		pager:  '#navegacao',
	    pagerAnchorBuilder: function(idx, slide) { 
	    	var img = slide.getElementsByTagName('img')[0].src.split('/').pop();
	    	return '<a href="#"><img src="_imgs/banners/thumbs/' + img + '" /></a>';
	    }
	});

	$('.fancy').fancybox();

	$('.abre-texto').click( function(e){
		e.preventDefault();

		var alvo = $('#'+$(this).attr('data-id'));

		if(alvo.hasClass('fechado')){
			$(this).addClass('ativo');
			alvo.removeClass('fechado');
		}else{
			$(this).removeClass('ativo');
			alvo.addClass('fechado');
		}
	});

	Modernizr.load([
		{
		  	test: Modernizr.borderradius,
		  	nope: ['js/jquery.corner.js' ,'js/polyfill-border.js']
		},
		{
			test: Modernizr.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);	
	
});